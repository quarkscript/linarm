# The Linux kernel and some other software compilations for specific arm devices
## Contents
- [xe303c12](#xe303c12)
  - [archlinuxarm](#archlinuxarm)
  - [void-linux](#void-linux)
  - [kali/devuan/bunsenlabs linux](#kalidevuanbunsenlabs-linux)
  - [patches, firmwares](#some-extra-patchesconfigsfirmwares) 
  - [recovery/install/test disk images](#recoveryinstalltest-disk-images)
  - some examples
    - [run in qemu](#example-of-run-under-hypervisor-qemu)
    - [build void-linux packages](#example-of-cross-compiling-void-linux-packages)
- [xe503](#xe503)
  - [archlinuxarm](xe503/archlinuxarm/linux_xe503)
  - [voidlinux](xe503/voidlinux/linux_xe503)
  - [kali/devuan linux](xe503/deb-linux)
    - [mesa 20.3.5 with partial hw-accel](#how-mesa-2035-was-rebuilded) (same as for xe303c12)
- [mx9pro](#mx9pro)
- ...
- [prebilded packages](#packages)

## xe303c12
It is an old armv7 [chromebook](https://www.samsung.com/us/business/support/owners/product/chromebook-xe303c12/). [Example](https://www.youtube.com/watch?v=hZt1fPso0e0) of running linux on it. 

### archlinuxarm
[link](xe303c12/archlinuxarm)

- [kernel](xe303c12/archlinuxarm/kernel) - Forked from [archlinux|ARM](https://github.com/archlinuxarm/PKGBUILDs/tree/master/core/linux-armv7). Archlinux|ARM package build script.      
- [firmware](xe303c12/archlinuxarm/firmware) - Archlinux|ARM package build script. It collect a couple of necessary files and saves some space compared to regular linux-firmware.

Some packages from [AUR](https://aur.archlinux.org/) could be installed by [sfslib](https://gitlab.com/quarkscript/arch_linux_scripts/-/blob/master/some-scripts/sfslib) like: `./sfslib armget 'pkg name'`

### void-linux
[link](xe303c12/void-linux)

The same as for archlinuxarm, but for void-linux. Second forked source is [void-packages](https://github.com/void-linux/void-packages/tree/master/srcpkgs/linux5.8). 
- [kernel](xe303c12/void-linux/linux_xe303c12) - Void-linux package build script, it can be cross-compiled with [xbps-src](https://github.com/void-linux/void-packages) into void-linux package with armv7hf-glibc or armv7hf-musl architectures.
- [firmware](xe303c12/void-linux/linux_xe303c12_firmware) - Void-linux package build script. It collect a couple of necessary files and  takes up less space than a regular linux-firmware package.

### kali/devuan/bunsenlabs linux
[link](xe303c12/deb)

To try Kali on that device you may use [official build script](https://gitlab.com/kalilinux/build-scripts/kali-arm) or [my rewrited mod of that script](xe303c12/deb/gadebli) or use already builded image

Devuan armhf linux run on this device too, you may build disk image on Devuan x86(_64) for it with parameter 'devuan'

My script could be used with linux kernel 5.15.12 like:
``` 
#!/bin/bash

# enable network
sudo dhclient eth0

# download kernel source
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.12.tar.xz --no-check-certificate

# download rcn-ee patch
wget https://rcn-ee.com/deb/sid-armhf/v5.15.0-rc7-armv7-x7/patch-5.15-rc7-armv7-x7.diff.gz

# download config
wget https://gitlab.com/quarkscript/linarm/-/raw/master/xe303c12/kali-linux/config

# download script
wget wget https://gitlab.com/quarkscript/linarm/-/raw/master/xe303c12/deb/gadebli

# make script executable
chmod +x gadebli

# extract patch
gzip -d patch-5.15-rc7-armv7-x7.diff.gz

# download kali injection patch
wget https://gitlab.com/kalilinux/build-scripts/kali-arm/-/raw/master/patches/kali-wifi-injection-5.9.patch

# unite patches
cat kali-wifi-injection-5.9.patch>>patch-5.15-rc7-armv7-x7.diff

# build kali-linux system, disk image and kernel; write build log
sudo ./gadebli patch-5.15-rc7-armv7-x7.diff btrfs |& tee -a build.log

## or build devuan-linux with xfce: system, disk image and kernel; write build log
#sudo ./gadebli patch-5.15-rc7-armv7-x7.diff btrfs devuan |& tee -a build.log

## rebuild kernel; write build log
#sudo ./gadebli patch-5.15-rc7-armv7-x7.diff ck |& tee -a build.log

## rebuild disk image; write build log
#sudo ./gadebli btrfs bdi |& tee -a build.log

## of rebuild devuan disk image; write build log
#sudo ./gadebli devuan btrfs bdi |& tee -a build.log

```
Since script was rewrited commands from [this demo](https://youtu.be/GCAjI37bUYo) may be differ but idea is the same

### Some extra patches/configs/firmwares

- [try to reduce default display backlight level](xe303c12/xe303c12_rsbl.patch)
- [try to disable wifi autosuspend](xe303c12/xe303c12_cwifiautosus.patch)
- [old wifi firmware](xe303c12/sd8797_uapsta.bin)
- [try to add maxtouch trackpad support](xe303c12/xe303c12_add_atmeltouch.patch) [atmel trackpad old fw](xe303c12/maxtouch.fw) [atmel trackpad old fw config](xe303c12/maxtouch.cfg)

> Since the path to dtbs was changed on kernel ver >= 6.5.x patches may require path updating like ` sed -i 's|dts\/exy|dts\/samsung\/exy|g' patch-name.patch `

### Recovery/install/test disk images
Disk images created for demonstration, testing and recovery things. For daily use user should tune it to his own needs and upsize it to used pendrive, sdcard and so on.
 [empty disk image maker](xe303c12/edim) can be used to create a disk image of the required size. [demonstration](https://youtu.be/ALJR2doOipc)

 [edim_nvubt](xe303c12/edim_nvubt) same as edim but with partitions scheme like for nv-uboot bootloader

 [empty disk maker](xe303c12/edm) same as edim script, but works with disk directly and do not create image

> Possible issue: recreating disk image with btrfs file system with the latest btrfs-tools can cause kernel panics and fail to boot. It's a matter of changes in the btrfs filesystems. Updating the kernel to the recent version (6.6.6 for example) should resolve this issue.

Created disk images:
- based on archlinux|ARM 
  - [very old, minimal, console only (old)](https://drive.google.com/u/0/uc?id=1O94t7i_gBygdlDLsbyp9D8q7T425sgpM&export=download)
  - [old, minimal, console only](https://drive.google.com/u/0/uc?id=1H7-WnYNJtQDMwrfvdIBnLnrswquFC1av&export=download)
  - [console only](https://drive.usercontent.google.com/download?id=14CCvgAYNS_76N7YJcz1cTG1WbB95O9B8&export=download&authuser=0) updated Sep 18, 2024. Login: root Pass: q
  - [with xfce4 DE](https://drive.google.com/u/0/uc?id=1yTRChwYdWFl-0z06p19kZ0leLGR2vEyp&export=download) . . . . . . [screenshot](_m/archlinuxarm_xfce4.png)
- based on Void-linux (for update, mesa and mesa-dri packages should be unholded, like xbps-pkgdb -m unhold 'package')
  - [glibc, minimal, console only](https://drive.google.com/u/0/uc?id=1m4GPpz1W4meUyvFn0byTxbK-zENSg96X&export=download)
  - [glibc, with xfce4 DE](https://drive.google.com/u/0/uc?id=1ylcaJ4aiyN4Zl5GqmietmcGmgIhWoElo&export=download)
  - [musl, minimal, console only](https://drive.google.com/u/0/uc?id=1H5JzkGVWHtZeh1a0CgZciRosZtVrSiZg&export=download)
  - [musl, with xfce4 DE](https://drive.google.com/u/0/uc?id=18ExLAz0M_gnjUF4PvsBnDo-0EnA4XZ6h&export=download) . . . . . . [screenshot](_m/void_musl_xfce4.png)
- based on debian linux
  - [Kali with xfce4 DE](https://drive.google.com/u/0/uc?id=1fW0oDNb44kXkEU_zrp_HtsZZtfphyetz&export=download)
  - [Devuan with xfce4 DE](https://drive.google.com/u/0/uc?id=1KSgE-3G2GEBzcYhk_JleVYWl6m061GdI&export=download)
  - [Devuan with lxqt DE](https://drive.google.com/u/0/uc?id=1Oil2eE-RrpP5I1kGCoTsw1JEqxeTgsNC&export=download) . . . . . . [screenshot](_m/devuan_lxqt_root.png)
  - [Devuan5 with bunsenlabs UI scripts](https://drive.google.com/u/0/uc?id=1-obTOWKIbgRjQZDjjQazbOtPFHboeA8N&export=download) . . . . . . [screenshot](_m/devuan-bunsen.png)
- based on alpine linux
  - [with xfce4 DE](https://drive.google.com/u/0/uc?id=108zWgPRvxzveNVtW6GChxux_jEziENZT&export=download) . . . . . . [screenshot](_m/alpine_xfce.png)
  - [with lxqt DE](https://drive.google.com/u/0/uc?id=17_-GTB9zZenqHc23TYacgqJI5Q8OTd0H&export=download)
- based on gentoo linux
  - [stage3 console disk image](https://github.com/quarkscript2/xe303c12_arm_linux/releases/tag/Gentoo_armv7hf_for_Google_Snow_Chromebook)

> Login and pass should be one of: user q , tester q , root q , root toor .

> Some systems may autologin to root account. Not all apps will run with root priveleges. My suggestion is to create a regular user for test/use it.

> The mesa version has been holded. This was a working solution for a few versions, but since then the mesa version has moved too far. To successfully update, the mesa version must be unholded. For archlinuxARM edit /etc/pacman.conf and remove mesa from line NoUpgrade = mesa; then update as usual. For void-linux execute sudo xbps-pkgdb -m unhold mesa mesa-dri; then update as usual.
Be aware, after update some apps will not work (like mpv) because of unsupported GPU by fresh mesa.

[contents](#contents)

### Example of run under hypervisor (qemu)
```
qemu-system-arm -machine virt,highmem=off -m 1024 -kernel zImage -append "root=/dev/vda2" -serial stdio -drive if=none,file=armv7hf_q.img,format=raw,id=hd0 -device virtio-blk-device,drive=hd0 -netdev user,id=net0 -device virtio-net-device,netdev=net0 
```
![](_m/example.gif)

 zImage could be extracted from second partition of disk image like:
```
mkdir dsk
sudo mount -t btrfs -o,loop,offset=$((512*40960)) armv7hf_q.img dsk
cp dsk/boot/zImage zImage
sudo umount dsk
```

### Example of cross-compiling Void-linux packages
(looks like xbps is no longer part of void-packages so this example requires Linux with xbps-static installed or Void-linux)
``` 
git clone https://github.com/void-linux/void-packages.git --depth=1
git clone https://gitlab.com/quarkscript/linarm.git --depth=1

cd void-packages/

## copy templates to src-dir
  cp -fr ../linarm/xe303c12/void-linux/linux_xe303c12 srcpkgs/
  cp -fr ../linarm/xe303c12/void-linux/linux_xe303c12_firmware srcpkgs/
    
##############################################################

## make glibc build env.
  ./xbps-src binary-bootstrap
  
## make glibc kernel package
  ./xbps-src -a armv7hf build linux_xe303c12
  ./xbps-src -a armv7hf pkg linux_xe303c12

## make glibc firmware package
  ./xbps-src -a armv7hf pkg linux_xe303c12_firmware

##############################################################

## make musl build env.
  ./xbps-src -m x86_64-musl binary-bootstrap -A x86_64-musl

## make musl kernel package
  ./xbps-src -m x86_64-musl -a armv7hf-musl build linux_xe303c12
  ./xbps-src -m x86_64-musl -a armv7hf-musl pkg linux_xe303c12 

## make musl firmware package
  ./xbps-src -m x86_64-musl -a armv7hf-musl pkg linux_xe303c12_firmware
  
```

 If you plan to re-sign kernel during installation then you will need to install a uboot-mkimage/uboot-tools firstly.


[contents](#contents)

## xe503
[link](xe503/)

Forked from xe303c12 but not tested; kernel config taken from archlinuxarm-github; instructions same as for [xe303c12](#xe303c12).

## mx9pro
RK3328 tvbox, 2/16 Gb version. [example](https://duckduckgo.com/?q=mx9pro+2%2F16G&t=ffab&iar=images&iax=images&ia=images)

- Debian based aarch64 kernel cross build [script](mx9pro/deb/gadebli)
- Alpine linux 3.19 aarch64 [kernel package](../../tree/pkg-krn-mx9?ref_type=heads), [kernel build scenario](mx9pro/alpine-linux/linux-mx9pro)

Some patches taken from [LibreElecTV](https://github.com/LibreELEC/LibreELEC.tv/tree/master/projects/Rockchip/patches/linux/default) some other patches taken from [Armbian](https://github.com/armbian/build/tree/main/patch/kernel)

[curious link](https://github.com/ayufan-rock64/linux-build/blob/master/recipes/overclocking.md)

## Packages

- armhf / armv7l packages compiled for cortex-a15 cpu with neon-vfpv4 capable fpu.
    - devuan 4 chimaera / debian 11 packages
        - [blender 2.79b](https://gitlab.com/quarkscript/linarm/-/tree/pkg_blender) [small demo](_m/blender_on_mali-t604.mkv) on system with patched mesa 20.3.5. [Fix](https://github.com/lgritz/OpenImageIO/commit/b6f5f92add4972ccb80047dc22df9d1f0eb0bc5c) for OpenImageIO headers on Devuan Chimaera, in case you want to build it by yourself. Blender source was taken from Devuan Beowolf (i.e. Debian 10)
        - [fritzing](https://gitlab.com/quarkscript/linarm/-/tree/pkg-fritzing?ref_type=heads) 1.0.1
        - [inkscape](https://gitlab.com/quarkscript/linarm/-/tree/pkg-inkscape?ref_type=heads) 1.2.2
        - [kicad](https://gitlab.com/quarkscript/linarm/-/tree/pkg-kicad?ref_type=heads) 7.0.5 ([cmake](https://gitlab.com/quarkscript/linarm/-/tree/pkg-cmake?ref_type=heads) 3.26.4, required for kicad build)
        - [linux-kernel-xe303c12-6.9.12.deb](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-kernel-xe303c12-6.9.12.deb?ref_type=heads) with BORE scheduler
            - [linux-kernel-xe303c12-6.9.10.deb](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-kernel-xe303c12-6.9.10.deb?ref_type=heads)
                - [linux-kernel-xe303c12-6.7.8.deb](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-kernel-xe303c12-6.7.8.deb?ref_type=heads)
                    - [linux-kernel-xe303c12-6.7.5.deb](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-kernel-xe303c12-6.7.5.deb?ref_type=heads)
                        - [linux-kernel-xe303c12-6.6.6.deb](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-kernel-xe303c12-6.6.6.deb?ref_type=heads)
                            - [linux-kernel-5.4.258-xe303c12-kvm.deb](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-kernel-5.4.258-xe303c12-kvm.deb?ref_type=heads) with KVM enabled, requre uboot that switch CPU to special mode, may boot as regular (w/o kvm), does not support last btrfs filesystem due to low kernel ver, so ext4 should be used as rootfs
        - [lite-xl](https://gitlab.com/quarkscript/linarm/-/blob/pkg-other/lite-xl_armhf.deb?ref_type=heads) 2.1.1
        - [mesa](https://gitlab.com/quarkscript/linarm/-/tree/pkg-mesa) with mali t604/t628 gpu acc patch
        - [neovim-0.9.4](https://gitlab.com/quarkscript/linarm/-/blob/pkg-other/neovim-0.9.4-1_armv7l.deb?ref_type=heads)
        - [ngspice 4](https://gitlab.com/quarkscript/linarm/-/tree/pkg-ngspace?ref_type=heads)
    - void-linux packages
        - [linux_xe303c12-6.9.12_1.armv7l.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.9.12_1.armv7l.xbps?ref_type=heads) with BORE schedeler
            - [linux_xe303c12-6.9.10_1.armv7l.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.9.10_1.armv7l.xbps?ref_type=heads)
                - [linux_xe303c12-6.7.8_1.armv7l.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.7.8_1.armv7l.xbps?ref_type=heads)
                    - [linux_xe303c12-6.6.6_1.armv7l.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.6.6_1.armv7l.xbps?ref_type=heads)
        - [linux_xe303c12-6.9.12_1.armv7l-musl.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.9.12_1.armv7l-musl.xbps?ref_type=heads) with BORE scheduler (alpine linux could work with it too in case of manually installed)
            - [linux_xe303c12-6.9.10_1.armv7l-musl.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.9.10_1.armv7l-musl.xbps?ref_type=heads)
                - [linux_xe303c12-6.7.8_1.armv7l-musl.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.7.8_1.armv7l-musl.xbps?ref_type=heads)
                    - [linux_xe303c12-6.6.6_1.armv7l-musl.xbps](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux_xe303c12-6.6.6_1.armv7l-musl.xbps?ref_type=heads)
    - archlinuxarm packages
        - [linux-xe303c12-6.9.12-1-any.pkg.tar.zst](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-xe303c12-6.9.12-1-any.pkg.tar.zst?ref_type=heads) with BORE scheduler
            - [linux-xe303c12-6.9.10-1-any.pkg.tar.zst](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-xe303c12-6.9.10-1-any.pkg.tar.zst?ref_type=heads)
                - [linux-xe303c12-6.7.8-1-any.pkg.tar.zst](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-xe303c12-6.7.8-1-any.pkg.tar.zst?ref_type=heads)
                    - [linux-xe303c12-6.6.6-1-any.pkg.tar.zst](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-xe303/linux-xe303c12-6.6.6-1-any.pkg.tar.zst?ref_type=heads)
- aarch64 rk3328
    - [alpine linux kernel](https://gitlab.com/quarkscript/linarm/-/blob/pkg-krn-mx9/linux-mx9pro-6.6.12-r0.apk)

### How neovim 0.9.4 was builded

Used [runenv](https://gitlab.com/quarkscript/arch_linux_scripts/-/blob/master/some-scripts/runenv) and [nvimmkpk](https://gitlab.com/quarkscript/linarm/-/blob/pkg-other/nvimmkpk?ref_type=heads) scripts

```
# run shell script that overlays build env
./runenv

# next commands should be passed to overlayed env
user="user"                                     # specify your login
cxx=' -O2 -ftracer -funroll-loops '             # specify extra flags if you want

# install deps
apt install  lua-mpack lua-lpeg libunibilium-dev libmsgpack-dev libtermkey-dev ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip curl doxygen

# not needed with updated runenv
#cd /build

# clone repo
sudo -u $user git clone https://github.com/neovim/neovim

cd neovim

# build and place near some deps
sudo -u $user make CMAKE_BUILD_TYPE=RelWithDebInfo CMAKE_INSTALL_PREFIX=/ CFLAGS+="$cxx" CXXFLAGS+="$cxx"

# switch to release
sudo -u $user git checkout stable

# build nvim
sudo -u $user make CMAKE_INSTALL_PREFIX=/ CMAKE_BUILD_TYPE=Release CFLAGS+="$cxx" CXXFLAGS+="$cxx"

# test nvim
sudo -u $user VIMRUNTIME=runtime ./build/bin/nvim

# install nvim
mkdir ../pkg
make install DESTDIR=/build/pkg

# exit from overlayed env
exit

# build deb package
sudo ./nvimmkpk
```

### How mesa 20.3.5 was rebuilded

Used [runenv](https://gitlab.com/quarkscript/arch_linux_scripts/-/blob/master/some-scripts/runenv) script

```
# run shell script that overlays build env
./runenv

# specify your login
user="user"

# not needed with updated runenv
#cd /build

# install deps and sources
apt install dpkg-dev devscripts
apt-get source libegl-mesa0 libegl1 libgl1-mesa-dri libglapi-mesa libglu1-mesa libglvnd0 libglx-mesa0 libglx0 mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers
apt build-dep libegl-mesa0 libegl1 libgl1-mesa-dri libglapi-mesa libglu1-mesa libglvnd0 libglx-mesa0 libglx0 mesa-va-drivers mesa-vdpau-drivers mesa-vulkan-drivers

cd mesa-*

# mesa 20.3.5 patch
echo "diff -Naur a/src/gallium/drivers/panfrost/pan_screen.c b/src/gallium/drivers/panfrost/pan_screen.c
--- a/src/gallium/drivers/panfrost/pan_screen.c	2021-12-29 21:05:19.000000000 +0000
+++ b/src/gallium/drivers/panfrost/pan_screen.c	2022-01-17 20:33:42.219447658 +0000
@@ -846,6 +846,10 @@
         /* Check if we're loading against a supported GPU model. */

         switch (dev->gpu_id) {
+        case 0x600: /* try an unsupported T60x */
+        case 0x604: /* try an unsupported T604 */
+        case 0x620: /* try an unsupported T62x */
+        case 0x628: /* try an unsupported T628 */
         case 0x720: /* T720 */
         case 0x750: /* T760 */
         case 0x820: /* T820 */
">gpuacc.patch

patch -p1 -i gpuacc.patch

# force cxx if needed
cxx=" -march=armv7-a+mp+neon-vfpv4 -mtune=cortex-a15 -O2 --param l1-cache-size=64 --param l2-cache-size=1024 -fno-strict-aliasing -fipa-sra -fivopts -fomit-frame-pointer -foptimize-sibling-calls -fpredictive-commoning -fprefetch-loop-arrays -frename-registers -fshrink-wrap-separate -fsimd-cost-model=unlimited -fsplit-loops -fsplit-paths -fssa-phiopt -fstdarg-opt -ftracer -ftree-cselim -ftree-dominator-opts -ftree-loop-if-convert -ftree-loop-optimize -ftree-loop-vectorize -funroll-loops -funswitch-loops -fvect-cost-model=unlimited -ftree-vectorize -mfpu=neon-vfpv4 "
echo 'diff -Naur a/debian/rules f/debian/rules
--- a/debian/rules	2023-03-09 10:53:56.000000000 +0000
+++ f/debian/rules	2023-03-09 11:53:39.900000646 +0000
@@ -17,16 +17,16 @@

 ifeq (,$(filter $(DEB_HOST_ARCH), armhf sh3 sh4))
 buildflags = \
-	$(shell DEB_CFLAGS_MAINT_APPEND=-Wall DEB_CXXFLAGS_MAINT_APPEND=-Wall dpkg-buildflags --export=configure)
+	$(shell DEB_CFLAGS_MAINT_APPEND=" -Wall '"$cxx"' " DEB_CXXFLAGS_MAINT_APPEND=" -Wall '"$cxx"' " dpkg-buildflags --export=configure)
 else
   ifneq (,$(filter $(DEB_HOST_ARCH), armhf))
   # Workaround for a variant of LP: #725126
   buildflags = \
-	$(shell DEB_CFLAGS_MAINT_APPEND="-Wall -fno-optimize-sibling-calls" DEB_CXXFLAGS_MAINT_APPEND="-Wall -fno-optimize-sibling-calls" dpkg-buildflags --export=configure)
+	$(shell DEB_CFLAGS_MAINT_APPEND="-Wall -fno-optimize-sibling-calls '"$cxx"' " DEB_CXXFLAGS_MAINT_APPEND="-Wall -fno-optimize-sibling-calls '"$cxx"' " dpkg-buildflags --export=configure)
   else
   # Workaround for https://gcc.gnu.org/bugzilla/show_bug.cgi?id=83143
   buildflags = \
-	$(shell DEB_CFLAGS_MAINT_APPEND="-Wall -O1" DEB_CXXFLAGS_MAINT_APPEND="-Wall -O1" dpkg-buildflags --export=configure)
+	$(shell DEB_CFLAGS_MAINT_APPEND="-Wall -O1 '"$cxx"' " DEB_CXXFLAGS_MAINT_APPEND="-Wall -O1 '"$cxx"' " dpkg-buildflags --export=configure)
   endif
 endif
'>flags.patch
# to force flags uncomment next line
#patch -p1 -i flags.patch

chown $user -R /build

sudo -u $user debuild -b -uc -us

# exit from overlayed env
exit

```

[contents](#contents)

>Be aware! Given scripts or packages are not officially supported by any mentioned Linux distributions.

> this repo has been reorganized to get an easier way of keeping from unlimited size grow
